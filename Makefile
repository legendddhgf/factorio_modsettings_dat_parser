#factorio mod settings utility By Isaak Cherdak
#based on info from https://wiki.factorio.com/Mod_settings_file_format

MAIN=factorio_msu
MODULES=property_tree
SRC=${MAIN:%=%.cpp} ${MODULES:%=%.cpp}
HDR=${MODULES:%=%.hpp}
OBJ=${SRC:%.cpp=%.o}
EXE=${MAIN}
COMP=g++ -Wall -pedantic -std=c++2a -c
LINK=g++ -lm -o

all: ${EXE}

${EXE}: obj ${OBJ}
	${LINK} ${EXE} ${OBJ}

obj: ${SRC} ${HDR}
	${COMP} ${SRC}

clean:
	rm -f ${OBJ}

spotless: clean
	rm -f ${EXE}

.PHONY: all obj clean spotless
