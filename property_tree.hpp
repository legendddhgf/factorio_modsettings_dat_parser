/*
 * factorio mod settings utility By Isaak Cherdak
 * based on info from https://wiki.factorio.com/Mod_settings_file_format
 */

#ifndef PROPERTY_TREE_H
#define PROPERTY_TREE_H

#include <stdint.h>
#include <stdio.h>

#define PTTNONE (0)
#define PTTBOOL (1)
#define PTTNUMBER (2)
#define PTTSTRING (3)
#define PTTLIST (4)
#define PTTDICT (5)

typedef struct PropertyTree {
  // type key
  // 0: none
  // 1: bool. Basically int8_t
  // 2: number. Basically a double
  // 3: string. If empty, bool (int8_t). Otherwise, uint8_t (len) and N int8_t
  // 4: List. Basically a dictionary
  // 5: Dictionary. uint8_t (len), len times: string (or empty) and PropertyTree
  uint8_t type;
  void *data;

  PropertyTree(uint8_t, void *, uint16_t);
  ~PropertyTree();
  void print(FILE *);
} PropertyTree;


#endif //PROPERTY_TREE_H
