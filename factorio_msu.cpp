/*
 * factorio mod settings utility By Isaak Cherdak
 * based on info from https://wiki.factorio.com/Mod_settings_file_format
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdint.h>

using namespace std;

void usage_exit(char *util_name) {
  printf("Usage: %s <dat file>\n", util_name);
  exit(1);
}

int main(int argc, char **argv) {
  if (argc < 2) usage_exit(argv[0]);
  char *datfile = argv[1];
  ifstream file_stream(datfile, ios::out | ios::binary);
  if (!file_stream) {
    printf("File doesn't exist: %s\n", datfile);
    exit(2);
  }
  uint8_t version[9];
  file_stream.read((char *) &version[0], 8);
  int major_ver = version[0] | (version[1] << 8);
  int minor_ver = version[2] | (version[3] << 8);
  int patch_ver = version[4] | (version[5] << 8);
  int dev_ver = version[6] | (version[7] << 8);
  printf("Version is %d.%d.%d (dev version is %d)\n",
      major_ver, minor_ver, patch_ver, dev_ver);



  file_stream.close();
  if (!file_stream.good()) {
    printf("%s had trouble being closed for reading\n", datfile);
    exit(3);
  }

  return 0;
}
