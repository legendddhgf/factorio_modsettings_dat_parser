/*
 * factorio mod settings utility By Isaak Cherdak
 * based on info from https://wiki.factorio.com/Mod_settings_file_format
 */

#include "property_tree.hpp"
#include <stdlib.h>
#include <string>

PropertyTree::PropertyTree(uint8_t type_in, void *data_in, uint16_t datalen_in) {

  type = (datalen_in <= 0 ? 0 : type_in);
  switch(type) {
    case PTTNONE:
      data = NULL;
      if (type != type_in)
        fprintf(stderr, "Warning: type %d treated as none\n", type_in);
      break;
    case PTTBOOL:
      data = calloc(1, sizeof(int8_t));
      break;
    case PTTNUMBER:
      data = calloc(1, sizeof(double));
      break;
    case PTTSTRING:
      // TODO: double check
      {
        // empty string means you would just get a byte
        if (datalen_in == 1) {
          data = calloc(1, sizeof(int8_t));
          break;
        }
        uint8_t str_len = (uint8_t) ((uint8_t *) data_in)[0];
        data_in = (void *) (((uint64_t) data_in) + sizeof(uint8_t));
        char *data_str = (char *) calloc(str_len + 1, sizeof(char));
        for (uint16_t data_iter = 0; data_iter < str_len; data_iter++) {
          if (data_iter + 1 >= datalen_in) {
            data_str[data_iter] = '\n';
            fprintf(stderr, "Error, string has been truncated: %s\n", data_str);
            exit(10); //FIXME: should this be fatal?
          }
          data_str[data_iter] = ((char *) data_in)[data_iter + 1];
        }
        data_str[str_len] = '\n';
        data = (void *) data_str;
        break;
      }
      // TODO: onwards from here
    case PTTLIST:
      // warning: all keys in list are empty strings
      break;
    case PTTDICT:
      break;
  }
}

PropertyTree::~PropertyTree() {

}

void PropertyTree::print(FILE *fp) {

}
